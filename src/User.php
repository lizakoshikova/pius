<?php

namespace App;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Validation;

class User
{
    private $id;

    private $name;

    private $email;

    private $password;

    private DateTime $dateTime;

    /**
     * @param $name
     * @param $email
     * @param $password
     */
    public function __construct($id, $name, $email, $password, DateTime $dateTime = new DateTime('now', new DateTimeZone('Europe/Moscow')))
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->dateTime = $dateTime;
    }

    private function idValidateProperties(){
        return [new NotBlank(message: "Id must be not a blank")];
    }
    private function nameValidateProperties(){
        return [new NotBlank(message: "Name must be not a blank"), new Length(['min' => 5])];
    }
    private function emailValidateProperties(){
        return [new NotBlank(message: "Email must be not a blank"), new Assert\Email(message: "The email {{ value }} is not a valid email.")];
    }
    private function passwordValidateProperties(){
        return [new NotBlank(message: "Password must be not a blank"), new Length(['min' => 12, 'minMessage'=> "Your password must be at least {{ limit }} characters long"])];
    }

    public function validate(){
        $violations = [];
        $validator = Validation::createValidator();
        $violations[] = $validator->validate($this->id, $this->idValidateProperties());
        $violations[] = $validator->validate($this->name, $this->nameValidateProperties());
        $violations[] = $validator->validate($this->email, $this->emailValidateProperties());
        $violations[] = $validator->validate($this->password, $this->passwordValidateProperties());
        return $violations;
    }

    public function getDateTime():DateTime{
        return $this->dateTime;
    }

    public function getDateTimeString(){
        return $this->dateTime->format("Y.m.d. h:i:s");
    }
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /////////

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    ////////


    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    ///////

    public function receivePassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }
}