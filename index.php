<?php
require_once 'vendor/autoload.php';

use App\User;
use App\Comment;

function userValidate(User $usr){
    $violationsFields = $usr->validate();
    if (0 !== count($violationsFields)) {
        foreach ($violationsFields as $violations) {
            foreach($violations as $violation)
                echo $violation->getMessage()."\n";
        }
    }
    echo "\n";
}

/*$validator = Validation::createValidator();
$usr = new User("", "1234", "", "1231");
$usr1 = new User(1, "Mikhail", "m@mail.com", "123412341234");
$usr2 = new User(2, "aaaaa", "email1com", "123412341234");
$usr3 = new User(3, "", "m@mail.com", "123123123123");

echo "User1 validation:\n";
userValidate($usr);
echo "User2 validation:\n";
userValidate($usr1);
echo "User3 validation:\n";
userValidate($usr2);
echo "User4 validation:\n";
userValidate($usr3);
*/
$usr4 = new User(1, "Mikhail", "1@mail.com", "123412341234", new DateTime('today'));
$usr5 = new User(2, "Egor", "2@mail.com", "123412341234", new DateTime('- 2 days'));
$usr6 = new User(3, "Maria", "3@mail.com", "123412341234", new DateTime('today'));
$usr7 = new User(4, "Elizaveta", "4@mail.com", "123412341234",  new DateTime('today'));
$comments = [];
$comments[] = new Comment(1, $usr4, "First comment!");
$comments[] = new Comment(2, $usr5, "Old comm!");
$comments[] = new Comment(3, $usr5, "Old comm!");
$comments[] = new Comment(4, $usr6, "First comment!");
$comments[] = new Comment(5, $usr7, "First comment!");


$targetDate = new DateTime('2023-03-16 00:00:00');
foreach ($comments as $comment){
    if ($targetDate <= $comment->getUser()->getDateTime()){
        print "user reg:". $comment->getUser()->getDateTimeString()."\n"."comment: ".$comment->getText()."\n";
    }
}
